﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCollider : MonoBehaviour
{
    [SerializeField] AudioClip loseLifeClip;
    GameLogic gameLogic;

    private void Awake()
    {
        gameLogic = FindObjectOfType<GameLogic>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            gameLogic.LoseLife();
            if (PlayerPrefs.GetInt("soundFX") == 1)
            {
                AudioSource.PlayClipAtPoint(loseLifeClip, collision.transform.position);
            }            
        }
    }
}
