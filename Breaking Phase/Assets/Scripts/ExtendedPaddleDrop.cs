﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendedPaddleDrop : MonoBehaviour
{
    public Paddle paddle;    
    public AudioClip missClip;

    private void Start()
    {
        paddle = FindObjectOfType<Paddle>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Paddle")
        {
            Destroy(gameObject);
            paddle.Extend();                        
        } else if (collision.gameObject.tag == "Lose Collider")
        {
            Destroy(gameObject);
            if (PlayerPrefs.GetInt("soundFX") == 1)
            {
                AudioSource.PlayClipAtPoint(missClip, transform.position);
            }
        }
    }    
}
