﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

    [SerializeField] bool autoPlay = false;
    [SerializeField] float smoothSpeed = .6f;
    [SerializeField] AudioClip stretch;
    [SerializeField] AudioClip shrink;

    float clampValue = 6.37f;

    Rigidbody2D myRigidBody;
    Ball ball;
    Animator animator;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        ball = FindObjectOfType<Ball>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.lossyScale.x == 2)
        {
            clampValue = 4.83f;
        } else
        {
            clampValue = 6.37f;
        }
        float mousePosInUnits = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);        

        if (Input.GetMouseButton(0) && !autoPlay)
        {
            paddlePos.x = Mathf.Clamp(mousePosInUnits, -clampValue, clampValue);
            transform.position = Vector2.Lerp(transform.position, paddlePos, smoothSpeed);
        } else if (autoPlay)
        {
            paddlePos.x = Mathf.Clamp(ball.transform.position.x, -6.37f, 6.37f);
            transform.position = paddlePos;
        }
    }

    public void Extend()
    {
        if (PlayerPrefs.GetInt("soundFX") == 1)
        {
            audioSource.clip = stretch;
            audioSource.pitch = 1.5f;
            audioSource.Play();
        }
        animator.Play("Extend Paddle");
    }    
}
