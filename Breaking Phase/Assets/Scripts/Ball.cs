﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Ball : MonoBehaviour
{
    [SerializeField] float xPush = .2f;
    [SerializeField] float yPush = 13f;
    [SerializeField] float stalePush = 2f;    
    [SerializeField] int staleCount = 0;
    [SerializeField] float lastX;
    [SerializeField] float lastY;
    [SerializeField] SettingsMenu settingsMenu;
    [SerializeField] float constantSpeed = 13f;
    [SerializeField] AudioClip hitClip;
    [SerializeField] GameObject hitParticles;

    bool hasStarted = false;    
    Vector2 vel;

    Rigidbody2D myRigidBody;
    Paddle paddle;
    LoseLifeText loseLifeText;
    TextMeshProUGUI loseLifeTextFilled;
    TextMeshProUGUI loseLifeTextOutline;
    

    private void Start()
    {
        AssignReferences();
        loseLifeTextOutline.enabled = false;
        loseLifeTextFilled.enabled = false;
        loseLifeText = FindObjectOfType<LoseLifeText>();
        settingsMenu = FindObjectOfType<SettingsMenu>();        
    }

    private void AssignReferences()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        paddle = FindObjectOfType<Paddle>();
        loseLifeTextFilled = GameObject.Find("Lose Life Text - Filled").GetComponent<TextMeshProUGUI>();
        loseLifeTextOutline = GameObject.Find("Lose Life Text - Outline").GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        bool settingsMenuOpen = settingsMenu.isActive();
        
        if (Input.GetMouseButtonUp(0) && !hasStarted && !settingsMenuOpen)
        {            
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);

            if (hit.collider != null && hit.collider.gameObject.tag == "Touch Space")
            {                
                hasStarted = true;
                myRigidBody.velocity += new Vector2(xPush, yPush);
            }
                        
        }

        if (!hasStarted)
        {
            transform.position = new Vector2(paddle.transform.position.x, (paddle.transform.position.y + 1f));
        }       
    }

    private void LateUpdate()
    {
        if (hasStarted)
        {
            myRigidBody.velocity = constantSpeed * (myRigidBody.velocity.normalized);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        constantSpeed += .1f;        
        float currentX = Mathf.Round(transform.position.x);
        float currentY = Mathf.Round(transform.position.y);

        if (currentX == lastX)
        {
            staleCount++;
            lastX = currentX;
            if (staleCount >= 4)
            {
                myRigidBody.velocity += new Vector2(stalePush, 0f);
                staleCount = 0;
            }
        } else if (currentY == lastY)
        {
            staleCount++;
            lastY = currentY;
            if (staleCount >= 4)
            {
                myRigidBody.velocity += new Vector2(0f, stalePush);
                staleCount = 0;
            }
        } else
        {
            lastX = currentX;
            lastY = currentY;
            staleCount = 0;
        }

        if (PlayerPrefs.GetInt("soundFX") == 1)
        {
            AudioSource.PlayClipAtPoint(hitClip, collision.gameObject.transform.position);
        }

        Instantiate(hitParticles, transform.position, Quaternion.identity);
    }    

    public bool HasStarted()
    {
        return hasStarted;
    }

    public void ResetBall()
    {
        constantSpeed = constantSpeed / 2;
        if (constantSpeed < 13)
        {
            constantSpeed = 13f;
        }
        StartCoroutine(DelayedBallReset());
    }

    public void ZeroVelocity()
    {
        myRigidBody.velocity = new Vector2(0, 0);
    }

    public void TogglePause()
    {
        if (vel == Vector2.zero)
        {
            vel = myRigidBody.velocity;
            myRigidBody.velocity = Vector2.zero;
        } else
        {
            myRigidBody.velocity = vel;
            vel = Vector2.zero;
        }
    }

    IEnumerator DelayedBallReset()
    {
        myRigidBody.velocity = new Vector2(0, 0);
        loseLifeTextFilled.enabled = true;
        loseLifeTextOutline.enabled = true;
        loseLifeText.PlayLoseLifeAnimation();
        yield return new WaitForSeconds(2);
        loseLifeTextFilled.enabled = false;
        loseLifeTextOutline.enabled = false;
        hasStarted = false;
    }    

}
