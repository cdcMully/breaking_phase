﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript : MonoBehaviour
{
    public AudioMixer audioMixer;

    private void Awake()
    {
        int musicManagerCount = FindObjectsOfType<AudioScript>().Length;
        if (musicManagerCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        audioMixer.SetFloat("masterVolume", PlayerPrefs.GetFloat("masterVolume"));

        if (!PlayerPrefs.HasKey("soundFX"))
        {
            PlayerPrefs.SetInt("soundFX", 1);
        }
    }
}
