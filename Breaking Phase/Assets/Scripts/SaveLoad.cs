﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoad : MonoBehaviour
{
    private string DATA_PATH = "/MyGame.dat";

    [SerializeField] private Player thePlayer = null;    

    private void Awake()
    {
        int saveLoadCount = FindObjectsOfType<SaveLoad>().Length;
        if (saveLoadCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        if (File.Exists(Application.persistentDataPath + DATA_PATH)) {
            LoadData();
        } else
        {
            SaveData();
            print("Data File Saved");
        }            
    }

    void SaveData()
    {
        FileStream file = null;

        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            file = File.Create(Application.persistentDataPath + DATA_PATH);

            Player mainPlayer = new Player(thePlayer.Name, thePlayer.CurrentLevel, thePlayer.CheckpointLevel);

            bf.Serialize(file, mainPlayer);

        } catch (Exception e)
        {
            if (e != null)
            {
                Debug.Log(e);
            }
        } finally
        {
            if (file != null)
            {
                file.Close();
            }
        }
    }

    void LoadData()
    {
        FileStream file = null;

        try
        {
            BinaryFormatter bf = new BinaryFormatter();

            file = File.Open(Application.persistentDataPath + DATA_PATH, FileMode.Open);

            thePlayer = bf.Deserialize(file) as Player;
        } catch (Exception e)
        {
            print(e);
        } finally
        {
            if (file != null)
            {
                file.Close();
            }
        }
    }

    public int GetCurrentLevel()
    {
        LoadData();
        return thePlayer.CurrentLevel;
    }

    public void SetCurrentLevel (int index)
    {
        thePlayer.CurrentLevel = index;        
        SaveData();        
    }
}
