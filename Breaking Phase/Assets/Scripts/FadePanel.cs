﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadePanel : MonoBehaviour
{
    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void FadeIn()
    {
        anim.Play("FadeIn");
    }

    public void FadeOut()
    {
        anim.Play("FadeOut");
    }
}
