﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Player
{
    private string _name;
    private int _currentLevel;
    private bool[] _checkpointLevel = new bool[11];    

    public Player() {}

    public Player(string name, int currentLevel, bool[] checkpointLevel)
    {
        _name = name;
        _currentLevel = currentLevel;
        _checkpointLevel = checkpointLevel;
    }

    public string Name
    {
        get
        {
            return _name;
        }
        set
        {
            _name = value;
        }
    }

    public int CurrentLevel
    {
        get
        {
            return _currentLevel;
        }
        set
        {
            _currentLevel = value;
        }
    }

    public bool[] CheckpointLevel
    {
        get
        {
            return _checkpointLevel;
        }
        set
        {
            _checkpointLevel = value;
        }
    }    
}
