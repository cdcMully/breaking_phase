﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraLifeDrop : MonoBehaviour
{
    [SerializeField] AudioClip extraLifeClip;
    [SerializeField] AudioClip noExtraLifeClip;

    GameLogic gameLogic;    

    private void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Paddle")
        {
            Destroy(gameObject);
            int currentLives = gameLogic.currentLives();
            if (currentLives < 5)
            {
                gameLogic.addLife();
                if (PlayerPrefs.GetInt("soundFX") == 1)
                {
                    AudioSource.PlayClipAtPoint(extraLifeClip, collision.transform.position);
                }                
            } else
            {
                if (PlayerPrefs.GetInt("soundFX") == 1)
                {
                    AudioSource.PlayClipAtPoint(noExtraLifeClip, collision.transform.position);
                }                
            }            
        } else if (collision.tag == "Lose Collider")
        {
            if (PlayerPrefs.GetInt("soundFX") == 1)
            {
                AudioSource.PlayClipAtPoint(noExtraLifeClip, collision.transform.position);
            }
            Destroy(gameObject);
        }        
    }
}
