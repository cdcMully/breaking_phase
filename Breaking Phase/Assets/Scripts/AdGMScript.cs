﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using System;

public class AdGMScript : MonoBehaviour
{
    private BannerView bannerView;

    private void Awake()
    {
        int AdGMCount = FindObjectsOfType<AdGMScript>().Length;
        if (AdGMCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
#if UNITY_ANDROID 
        string appId = "ca-app-pub-9148176362073472~9502544875";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-9148176362073472~2948840278";
#else
        string appId = "unexpected_platform";
#endif

        MobileAds.Initialize(appId);

        RequestBanner();
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-9148176362073472/5996336715";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-9148176362073472/3756958248";
#else
        string adUnitId = "unexpected_platform";
#endif

        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        AdRequest request = new AdRequest.Builder().AddTestDevice("CDC89E688D1E968D9850B6A02A90E04A").AddTestDevice("f37eac97c38f2fd5b6c3543046a281ca").Build();

        bannerView.LoadAd(request);
    }
}
