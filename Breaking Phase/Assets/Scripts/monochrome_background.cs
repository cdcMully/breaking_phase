﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monochrome_background : MonoBehaviour
{
    Animator anim;
    bool hasReset = false;
    
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        int bgAnimation = PlayerPrefs.GetInt("bgAnimation");
        if (PlayerPrefs.GetInt("bgAnimation") == 1)
        {
            PlayRegularAnimation();
        } else if (!hasReset)
        {
            PlayResetAnimation();
        }
    }

    private void PlayRegularAnimation()
    {        
        anim.Play("Monochrome Background Move");
        hasReset = false;
    }

    private void PlayResetAnimation()
    {        
        anim.Play("Monochrome Background Reset");
        hasReset = true;
    }
}
