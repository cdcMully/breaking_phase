﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBoss : MonoBehaviour
{
    Animator anim;
    SpriteRenderer sprite;
    bool isHit = false;
    [SerializeField] Sprite hitSprite;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();    

        StartCoroutine(GhostBossCo());
    }

    void FixedUpdate()
    {
        if (transform.lossyScale.x < .9f)
        {
            Move();
        }   
    }

    private void LateUpdate()
    {
        if (isHit)
        {
            StartCoroutine(HitSpriteCo());
            isHit = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {        
        if (collision.gameObject.name == "Ball")
        {
            isHit = true;                       
        }        
    }

    private void Move()
    {
        
        Vector2 newPos = new Vector2(UnityEngine.Random.Range(-6, 6), UnityEngine.Random.Range(9.5f, 3.5f));
        transform.position = newPos;
    }

    IEnumerator GhostBossCo()
    {
        //anim.isActiveAndEnabled("true");
        anim.Play("Fade and Surprise");
        yield return new WaitForSeconds(10);
        StartCoroutine(GhostBossCo());
    }

    IEnumerator HitSpriteCo()
    {        
        Sprite idleSprite = sprite.sprite;
        sprite.sprite = hitSprite;
        yield return new WaitForSeconds(1f);
        sprite.sprite = idleSprite;        
    }
}
