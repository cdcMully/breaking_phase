﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StormBoss : MonoBehaviour
{
    [SerializeField] GameObject[] strikes;
    [SerializeField] AudioClip[] thunderClips;
    [SerializeField] AudioClip rainClip;
    [SerializeField] float strikeFlashDelay = .8f;
    [SerializeField] float strikeDestroyDelay = 1f;
    [SerializeField] int strikeFrequency = 10;
    [SerializeField] int maxHits = 35;

    private int currentHits = 0;
    private bool isDestroyed = false;
    private GameLogic gameLogic;
    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private AudioSource[] oneShots;
    private bool toggleMute;


    private void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        gameLogic.addBlock();

        AudioSource.PlayClipAtPoint(rainClip, transform.position, .2f);

        if (PlayerPrefs.GetInt("soundFX") == 0)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0f;
            }
            toggleMute = true;
        }

        StartCoroutine(LightningStrikeCo());
    }

    private void FixedUpdate()
    {
        if (PlayerPrefs.GetInt("soundFX") == 1 && toggleMute)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0.2f;
            }
            toggleMute = false;
        }
        else if (PlayerPrefs.GetInt("soundFX") == 0 && !toggleMute)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0f;
            }
            toggleMute = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            currentHits++;
            if (currentHits >= maxHits)
            {
                DestroyBlock();
            }
            else
            {
                StartCoroutine(alphaDrop());
            }            
        }
    }

    IEnumerator LightningStrikeCo()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(1, 7));
        thunderSound();
        int index = UnityEngine.Random.Range(1, 6);
        switch (index)
        {
            case 1:
                StartCoroutine(Strike1());
                break;

            case 2:
                StartCoroutine(Strike2());
                break;

            case 3:
                StartCoroutine(Strike3());
                break;

            case 4:
                StartCoroutine(Strike4());
                break;

            case 5:
                StartCoroutine(Strike5());
                break;            
        }

        StartCoroutine(LightningStrikeCo());
    }

    private IEnumerator Strike1()
    {
        GameObject strike = Instantiate(strikes[0], new Vector2(3.75f, 1.6f), Quaternion.identity);
        yield return new WaitForSeconds(strikeFlashDelay);
        strike.GetComponent<Strike>().Flash();
        yield return new WaitForSeconds(strikeDestroyDelay);
        Destroy(strike);
    }

    private IEnumerator Strike2()
    {
        GameObject strike = Instantiate(strikes[1], new Vector2(2.4f, 1.3f), Quaternion.identity);
        yield return new WaitForSeconds(strikeFlashDelay);
        strike.GetComponent<Strike>().Flash();
        yield return new WaitForSeconds(strikeDestroyDelay);
        Destroy(strike);
    }

    private IEnumerator Strike3()
    {
        GameObject strike = Instantiate(strikes[2], new Vector2(-0.2f, 1.54f), Quaternion.identity);
        yield return new WaitForSeconds(strikeFlashDelay);
        strike.GetComponent<Strike>().Flash();
        yield return new WaitForSeconds(strikeDestroyDelay);
        Destroy(strike);
    }

    private IEnumerator Strike4()
    {
        GameObject strike = Instantiate(strikes[3], new Vector2(-2.2f, 1.25f), Quaternion.identity);
        yield return new WaitForSeconds(strikeFlashDelay);
        strike.GetComponent<Strike>().Flash();
        yield return new WaitForSeconds(strikeDestroyDelay);
        Destroy(strike);
    }

    private IEnumerator Strike5()
    {
        GameObject strike = Instantiate(strikes[4], new Vector2(-4.0f, 1.6f), Quaternion.identity);
        yield return new WaitForSeconds(strikeFlashDelay);
        strike.GetComponent<Strike>().Flash();
        yield return new WaitForSeconds(strikeDestroyDelay);
        Destroy(strike);
    }

    private void DestroyBlock()
    {
        PlayAnimation();
        Destroy(gameObject, .25f);
        gameLogic.subtractBlock();
    }

    private void PlayAnimation()
    {
        animator.Play("Block_Destroy");
    }

    private IEnumerator alphaDrop()
    {
        Color tmp = spriteRenderer.color;
        tmp.a = .5f;
        spriteRenderer.color = tmp;

        yield return new WaitForSeconds(.1f);

        tmp.a = 1f;
        spriteRenderer.color = tmp;
    }

    private void thunderSound()
    {
        if (PlayerPrefs.GetInt("soundFX") == 1)
        {
            AudioSource.PlayClipAtPoint(thunderClips[UnityEngine.Random.Range(0, 2)], transform.position);
        }
    }
}
