﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitScript : MonoBehaviour
{
    [SerializeField] int maxHits = 1;
    [SerializeField] int currentHits = 0;

    BombBoss bomb;
    GameLogic gameLogic;
    LevelManager levelManager;
    Animator animator = null;
    
    private void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        levelManager = FindObjectOfType<LevelManager>();
        animator = GetComponent<Animator>();
        bomb = GetComponent<BombBoss>();

        gameLogic.addBlock();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            currentHits++;
            if (currentHits >= maxHits)
            {
                DestroyBlock();
            }
        }
    }

    private void DestroyBlock()
    {
        gameLogic.subtractBlock();
        PlayAnimation();
        Destroy(gameObject, .25f);
    }

    private void PlayAnimation()
    {
        animator.Play("Block_Destroy");
    }
}
