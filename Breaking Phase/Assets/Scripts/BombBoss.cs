﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBoss : MonoBehaviour
{
    [SerializeField] float delay = 2f;
    [SerializeField] Sprite[] spriteArray;
    [SerializeField] AudioClip sizzle;
    [SerializeField] AudioClip boom;

    private int index = 0;
    private SpriteRenderer sprite;
    private LevelManager levelManager;
    private Collider2D col;    
    private AudioSource[] oneShots;
    private bool toggleMute;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        levelManager = FindObjectOfType<LevelManager>();
        col = GetComponent<Collider2D>();

        AudioSource.PlayClipAtPoint(sizzle, transform.position, .2f);

        if (PlayerPrefs.GetInt("soundFX") == 0)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0f;
            }
            toggleMute = true;
        }

        StartCoroutine(BombCo());        
    }

    private void FixedUpdate()
    {
        if (PlayerPrefs.GetInt("soundFX") == 1 && toggleMute)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0.2f;
            }
            toggleMute = false;
        } else if (PlayerPrefs.GetInt("soundFX") == 0 && !toggleMute)
        {
            oneShots = FindObjectsOfType<AudioSource>();
            foreach (AudioSource obj in oneShots)
            {
                obj.volume = 0f;
            }
            toggleMute = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            StartCoroutine(alphaDrop());
        }
    }

    private IEnumerator BombCo()
    {        
        if (index == 21)
        {
            col.enabled = false;
            if (PlayerPrefs.GetInt("soundFX") == 1)
            {
                AudioSource.PlayClipAtPoint(boom, transform.position, 1f);
            }
            yield return new WaitForSeconds(2);
            levelManager.GameOver();
        } else
        {
            yield return new WaitForSeconds(delay);
           sprite.sprite = spriteArray[index];
            index++;
            StartCoroutine(BombCo());
        }        
    }

    IEnumerator alphaDrop()
    {
        Color tmp = sprite.color;
        tmp.a = .5f;
        sprite.color = tmp;

        yield return new WaitForSeconds(.1f);

        tmp.a = 1f;
        sprite.color = tmp;
    }

    public void StopAnimation()
    {
        StopCoroutine(BombCo());        
    }
}
