﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseLifeText : MonoBehaviour
{
    public void PlayLoseLifeAnimation()
    {
        GetComponent<Animator>().Play("Lose Life Animation");
    }
}
