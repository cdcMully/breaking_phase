﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelManager : MonoBehaviour
{
    Ball ball;
    TextMeshProUGUI currentLevelDisplay;    
    int highestCompletedLevel;
    SaveLoad saveLoad;
    FadePanel fadePanel;

    private void Start()
    {
        ball = FindObjectOfType<Ball>();
        saveLoad = FindObjectOfType<SaveLoad>();
        fadePanel = FindObjectOfType<FadePanel>();
        
        if (SceneManager.GetActiveScene().name != "Start Menu")
        {
            currentLevelDisplay = GameObject.Find("Level Text").GetComponent<TextMeshProUGUI>();
            currentLevelDisplay.text = "Level - " + SceneManager.GetActiveScene().buildIndex.ToString();
        }        
    }

    public void LoadNextLevel()
    {           
        if (SceneManager.GetActiveScene().name != "Start Menu")
        {
            highestCompletedLevel = SceneManager.GetActiveScene().buildIndex;
            ball.ZeroVelocity();
            saveCurrentLevel();
        }
        StartCoroutine(DelayLoadNextLevel());        
    }

    public void GameOver()
    {
        SceneManager.LoadScene("Game Over");        
    }

    public void RestartGame()
    {
        Destroy(FindObjectOfType<GameLogic>().gameObject);
        SceneManager.LoadScene("Start Menu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public int GetSceneIndex()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

    public void Continue()
    {
        int currentLevelLoad = saveLoad.GetCurrentLevel();
        for (int index = 0; index <=5; index++)
        {
            if ((currentLevelLoad / 10) < index)
            {
                SceneManager.LoadScene("Level_" + ((index - 1) * 10 + 1).ToString());
                break;
            } else if (currentLevelLoad == 50)
            {
                SceneManager.LoadScene("Level_50");
                break;
            }
        }
    }

    public bool IsDivisible(int levelNum)
    {
        float remainder = levelNum % 10;
        if (remainder != 0)
        {
            return false;
        } else
        {
            return true;
        }
    }    

    public int currentPhase()
    {
        int returnIndex = 0;
        int currentBI = SceneManager.GetActiveScene().buildIndex;
        for (int index = 0; index <=5; index++)
        {
            if (currentBI /10 < index)
            {
                returnIndex = index;
                break;
            }
        }        
        return returnIndex;
    }

    private void saveCurrentLevel()
    {
        saveLoad.SetCurrentLevel(highestCompletedLevel);
    }

    public void startButton()
    {        
        highestCompletedLevel = 0;            
        saveCurrentLevel();
        SceneManager.LoadScene("Level_1");
    }

    IEnumerator DelayLoadNextLevel()
    {
        //fadePanel.FadeOut();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        //fadePanel.FadeIn();
    }
    
}
