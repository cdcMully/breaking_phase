﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeMarker : MonoBehaviour
{
    [SerializeField] Sprite crossedOutBall = null;

    public void crossOut()
    {        
        GetComponent<SpriteRenderer>().sprite = crossedOutBall;
                
    }    
}
