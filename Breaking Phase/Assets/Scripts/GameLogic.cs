﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class GameLogic : MonoBehaviour
{
    [SerializeField] int remainingBlocks = 0;        
    [SerializeField] GameObject lifeMarker = null;    
    [SerializeField] GameObject lifeMarkerParent;
    [SerializeField] float spacer = .7f;

    int lives = 2;
    bool levelOver = false;
    LevelManager levelManager;
    Ball ball;    
    List<GameObject> lifeMarkers = new List<GameObject>();

    private void Awake()
    {
        int gameLogicCount = FindObjectsOfType<GameLogic>().Length;
        if (gameLogicCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }        
    }

    private void Start()
    {        
        levelManager = FindObjectOfType<LevelManager>();        
        ball = FindObjectOfType<Ball>();
        lifeMarkerParent = GameObject.Find("Life Markers");
        drawLifeMarkers();
    }

    private void drawLifeMarkers() 
    {
        foreach (GameObject marker in lifeMarkers)
        {
            Destroy(marker.gameObject);
        }

        lifeMarkers.Clear();

        for (int index = 0; index < lives; index++)
        {
            Vector2 pos = new Vector2(-7.5f, -5.25f);
            if (index == 0)
            {
                lifeMarkers.Add(Instantiate(lifeMarker, pos, Quaternion.identity));
                lifeMarkers[index].transform.SetParent(lifeMarkerParent.transform);
            } else
            {
                lifeMarkers.Add(Instantiate(lifeMarker, pos + new Vector2(index * spacer, 0f), Quaternion.identity));
                lifeMarkers[index].transform.SetParent(lifeMarkerParent.transform);
            }         
            
        }
    }

    public void addBlock()
    {
        remainingBlocks++;
        levelOver = false;
    }

    public void subtractBlock()
    {        
        remainingBlocks--;        

        if (levelManager == null)
        {
            levelManager = FindObjectOfType<LevelManager>();
        }

        if (remainingBlocks <= 0)
        {
            levelOver = true;
            levelManager.LoadNextLevel();
        }
    }    

    public void addLife()
    {
        lives++;
        drawLifeMarkers();
    }

    public void LoseLife()
    {
        lives--;

        if (ball == null)
        {
            ball = FindObjectOfType<Ball>();
        }

        if (levelManager == null)
        {
            levelManager = FindObjectOfType<LevelManager>();
        }

        if (lives != 0)
        {            
            drawLifeMarkers();
            ball.ResetBall();
        } else
        {
            levelManager.GameOver();
        }               
    }

    public bool isLevelOver()
    {
        return levelOver;
    }

    public int currentLives()
    {
        return lives;
    }

}
