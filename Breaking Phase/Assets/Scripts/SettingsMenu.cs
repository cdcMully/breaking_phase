﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public GameObject settingsMenu;
    public GameObject creditsMenu;
    public Slider slider;
    public Toggle toggle;
    public Toggle soundFXToggle;
    public Ball ball;

    private void Awake()
    {
        if (ball == null)
        {
            ball = FindObjectOfType<Ball>();
        }        

        slider.value = PlayerPrefs.GetFloat("masterVolume");

        int bgAnimation = PlayerPrefs.GetInt("bgAnimation");

        int soundFX = PlayerPrefs.GetInt("soundFX");

        if (soundFX == 1)
        {
            soundFXToggle.isOn = true;
        } else
        {
            soundFXToggle.isOn = false;
        }

        
        if (bgAnimation == 1)
        {
            toggle.isOn = true;
        } else
        {
            toggle.isOn = false;
        }
        
    }

    public void Mute(float volume)
    {
        audioMixer.SetFloat("masterVolume", volume);
        PlayerPrefs.SetFloat("masterVolume", volume);        
    }

    public void MuteSoundFX(bool muteFX)
    {
        if (muteFX)
        {
            PlayerPrefs.SetInt("soundFX", 1);            
        } else
        {
            PlayerPrefs.SetInt("soundFX", 0);            
        }
    }

    public void ToggleSettingsMenu()
    {
        if (settingsMenu.activeSelf)
        {
            settingsMenu.SetActive(false);
        } else
        {
            settingsMenu.SetActive(true);
        }

        if (ball == null)
        {
            ball = FindObjectOfType<Ball>();
        }
        
        ball.TogglePause();
        
    }

    public void ToggleCreditsMenu()
    {
        if (creditsMenu.activeSelf)
        {
            creditsMenu.SetActive(false);
        }
        else
        {
            creditsMenu.SetActive(true);
        }

    }

    public void ToggleBGAnimation(bool value)
    {
        if (value)
        {
            PlayerPrefs.SetInt("bgAnimation", 1);
            print("PlayerPrefs Set As: " + PlayerPrefs.GetInt("bgAnimation"));
        } else
        {
            PlayerPrefs.SetInt("bgAnimation", 0);
            print("PlayerPrefs Set As: " + PlayerPrefs.GetInt("bgAnimation"));
        }
    }

    public bool isActive()
    {
        if (settingsMenu.activeSelf)
        {
            return true;
        } else
        {
            return false;
        }
    }
}
