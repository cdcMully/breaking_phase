﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    [SerializeField] int maxHits = 1;
    [SerializeField] int currentHits = 0;
    [SerializeField] bool isDestroyed = false;
    [SerializeField] Sprite[] sprites = null;
    [SerializeField] GameObject extraLifeDrop = null;
    [SerializeField] GameObject extendedPaddleDrop = null;
    [SerializeField] AudioClip[] breakClips = null;

    GameLogic gameLogic;
    Animator animator;
    SpriteRenderer spriteRenderer;
    LevelManager levelManager;
        
    
    void Start()
    {
        gameLogic = FindObjectOfType<GameLogic>();
        gameLogic.addBlock();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        levelManager = FindObjectOfType<LevelManager>();

        if (gameObject.name == "second_boss_red_paddle")
        {
            StartCoroutine(redPaddleBossCo());
        }
    }    

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            currentHits++;
            if (currentHits >= maxHits)
            {
                DestroyBlock();
            } else if (gameObject.tag == "Boss" && gameObject.name != "Ghost Boss")
            {
                StartCoroutine(alphaDrop());
            } else if (gameObject.name != "Ghost Boss")
            {
                spriteRenderer.sprite = sprites[currentHits - 1];
            }           
        }
    }

    private void DestroyBlock()
    {
        if (!isDestroyed)
        {
            isDestroyed = true;
            gameLogic.subtractBlock();
            PlayAnimation();
            Destroy(gameObject, .25f);
            if (PlayerPrefs.GetInt("soundFX") == 1)
            {
                AudioSource.PlayClipAtPoint(breakClips[UnityEngine.Random.Range(0, 2)], transform.position);
            }
            if (!gameLogic.isLevelOver())
            {
                lifeDrop();
            }
        }
    }

    private void PlayAnimation()
    {
        animator.Play("Block_Destroy");        
    }

    private void lifeDrop()
    {
        Vector2 pos = gameObject.transform.position;
        int currentPhase = levelManager.currentPhase();
        float randIndex = UnityEngine.Random.Range(1, 101);
        int currentLives = gameLogic.currentLives();
        
        if(currentLives >= 5)
        {            
            
        } else if (randIndex >= 95)
        {
            GameObject lifeDrop = Instantiate(extraLifeDrop, pos, Quaternion.identity);
            lifeDrop.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -5f);            
        } else if (randIndex >= 85 && currentPhase > 2)
        {
            
            GameObject extendedDrop = Instantiate(extendedPaddleDrop, pos, Quaternion.identity);
            extendedDrop.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, -5f);            
        }
    }

    IEnumerator alphaDrop()
    {
        Color tmp = spriteRenderer.color;
        tmp.a = .5f;
        spriteRenderer.color = tmp;

        yield return new WaitForSeconds(.1f);

        tmp.a = 1f;
        spriteRenderer.color = tmp;
    }

    IEnumerator redPaddleBossCo()
    {
        animator.Play("Red Boss Paddle Animation");
        yield return new WaitForSeconds(5);
        StartCoroutine(redPaddleBossCo());
    }
}
